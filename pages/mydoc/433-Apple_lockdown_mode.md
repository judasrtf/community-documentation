---
title: Apple Lockdown Mode
keywords: ios, pegasus, Apple, mac, ipad, lockdown, spyware
last_updated: October 4, 2022
tags: [Devices_Data_Security, articles]
summary: "Information about Apple Lockdown Mode, what we think about it, how to enable it, and usability constraints"
sidebar: mydoc_sidebar
permalink: 433-Apple_lockdown_mode.html
folder: mydoc
conf: Public
ref: Apple_lockdown_mode
lang: en
---


# Apple Lockdown Mode
## Information about Apple Lockdown Mode, what we think about it, how to enable it, and usability constraints

### Problem

Defending against targeted malware and spyware is a challenging task, due to the proliferation of mercenary spyware that utilizes sophisticated attacks (including exploitation of zero-day vulnerabilities) to compromise devices of users at-risk.

These targeted attacks are often directed at users' mobile devices, since they usually store a large amount of sensitive data, including records of private conversations, contact lists, passwords, documents, etc. In some instances, threat actors also turn on the device's microphone and cameras to spy on victims. Attackers also prioritize being stealthy to keep victims from noticing the attack.

Even when users follow security best practices and keep their operating system up to date, they can still be victims of compromises that exploit un-patched security vulnerabilities affecting core elements of the OS (see this [forensic report by Amnesty](https://www.amnesty.org/en/latest/research/2021/07/forensic-methodology-report-how-to-catch-nso-groups-pegasus/) as an example). Since vulnerabilities are inherent to software, it becomes difficult to defend against them, especially when the surface of attack grows through additional features and elements that are regularly added to operating systems.

Taking this into account, Apple developed Lockdown Mode to mitigate this problem. Lockdown Mode introduces a series of protections that try to reduce the attack surface of Apple devices and reduce the effectiveness of targeted attacks against Apple operating systems.

* * *

### Solution


#### What is Lockdown Mode?

Lockdown Mode is an optional protection for users that face targeted threats to their digital security. Lockdown Mode further hardens device defenses and strictly limits certain functionalities, reducing the attack surface that could be exploited by highly targeted mercenary spyware.

At launch, Lockdown Mode includes the [following protections](https://www.Apple.com/newsroom/2022/07/Apple-expands-commitment-to-protect-users-from-mercenary-spyware):

* **Messages:** Most message attachment types other than images are blocked. Some features, like link previews, are disabled.
* **Web browsing:** Certain complex web technologies, like just-in-time (JIT) JavaScript compilation, are disabled unless the user excludes a trusted site from Lockdown Mode.
* **Apple services:** Incoming invitations and service requests, including FaceTime calls, are blocked if the user has not previously sent the initiator a call or request.
* **Shared Albums:** All Shared Albums in Photos are removed, and any new invitations for Shared Albums are blocked.
* Wired connections with a computer or accessory are blocked when iPhone is locked.
* Configuration profiles cannot be installed, and the device cannot enroll into mobile device management (MDM), while Lockdown Mode is turned on.

Apple will continue to strengthen Lockdown Mode and add new protections to it over time.

#### Helpline & Community impressions

As stated above, protecting Helpline beneficiaries from targeted attacks, especially those that exploit zero-day vulnerabilities, is a complex challenge. With this in mind, protections like those offered by Apple Lockdown Mode are welcomed. Additionally, in consultation with partners who have done research into targeted attacks against Apple devices, some of the features and limitations that are introduced by Lockdown Mode directly respond to attack vectors that have been successfull in the past.

It is worth noting that Apple devices are not accessible to all Helpline beneciaries, and often have a cost that is prohibitive (especially in their more premium models). In addition to this, Apple has a questionable record on [some privacy related decisions](https://www.accessnow.org/Apple-encryption-expanded-protections-children/) and [product features](https://www.wired.com/story/opinion-Apples-air-tags-are-a-gift-to-stalkers/).

At the Helpline, we will continue to support and prioritize solutions that are based on Free and Open Source Software. However, when it comes to mobile device operating systems, the FOSS alternatives are limited, and require a significant amount of effort, resources and expertise to deploy and maintain. Considering this, Apple Lockdown is an adequate solution to recommend to Helpline beneficiaries who may be targeted by sophisticated attacks due to their threat model.

#### How to enable Lockdown

Lockdown Mode is disabled by default and can be enabled on an iPhone by going to Settings > Privacy and Security > Lockdown Mode. Turning it on or off requires a system reboot and the device's PIN.

#### Important notes and usability constraints

Given the fact that some web technologies are disabled in Lockdown Mode, websites can easily identify if Lockdown Mode is being used. This means that an adversary can identify if a potential victim is running Lockdown Mode, singling out the fact that more advanced protections for the OS are enabled. This could potentially push attackers to exploit vulnerabilities in 3rd party applications.

Additionally, when Lockdown Mode is enabled, some features will be disabled or limited, which can affect the way a device is used. This includes:

- **Browsing:** When Lockdown Mode is enabled, rendering of certain type of files within the browser will be disabled. This includes, for example, PDF files and some image files like SHI, BMP (24 bits), Targa, etc. PDFs can still be viewed on the phone, they just need to be downloaded first. It also affects custom fonts. Additionally, [WebGL](https://en.wikipedia.org/wiki/WebGL) will no longer be supported. This can affect how interactive websites are rendered. [WebRTC](https://en.wikipedia.org/wiki/WebRTC) is also disabled when Lockdown Mode is enabled. Some HTML5 features will also be disabled in Lockdown Mode, as well as rendering for some image types. A more in-depth analysis if the browser limitations is available [here](https://www.sevarg.net/2022/07/20/ios16-lockdown-mode-browser-analysis/).
- **Lockdown Bypass list:** Lockdown Mode can be bypassed for specific websites when using Safari web browser. To to do this, launch Safari, go to the website you want to exclude, hit AA -> Website Settings, toggle off Lockdown Mode and tap Turn Off. The banner at the top of the website should now read Lockdown Off.
- **Performance:** When Lockdown Mode is enabled, Just In Time compilation (JIT) will be disabled. It is important to note that JIT compilation fundamentally uses executable data, and thus poses security challenges and possible exploits, hence it being disabled by Lockdown Mode. JIT is widely use by javascript engines, and it being disabled in Lockdown Mode means that javascript will take significantly longer to load on websites. As stated above, for trusted websites, it is possible for the user to create exceptions to bypass Lockdown Mode.
- **Messaging:** Most message attachment types other than images are blocked. Some features, like link previews, are disabled.
- **Battery:** Due to the limitations detailed above, particularly those related to browsing, there may be additional stress on the phone's processor, which can cause faster battery drain. During our initial testing, this was not significant but will depend largely on the usage of the phone.
- **Photo album sharing:** When lockdown mode is enabled, shared albums will be removed from the Photos app, and new Shared Albums invitations will be blocked. 

* * *

### Comments

- [Excellent blog post with information on Lockdown Mode](https://www.sevarg.net/2022/07/20/ios16-lockdown-mode-browser-analysis/)
- [Library to detect whether a website viewer is running iOS with Lockdown Mode enabled](https://github.com/botherder/is-lockdown)


* * *

### Related Articles

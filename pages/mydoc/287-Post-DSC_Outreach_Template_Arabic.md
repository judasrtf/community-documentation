---
title: Post-DSC Arabic Outreach Template
keywords: email templates, outreach, events, DSC, digital security clinics
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "Arabic email template for outreach - addressed at members of civil society we met during a Digital Security Clinic or an event"
sidebar: mydoc_sidebar
permalink: 287-Outreach_Template_Arabic.html
folder: mydoc
conf: Public
ref: Post-DSC_Outreach_Template
lang: ar
---


# Post-DSC Arabic Outreach Template
## Email template for outreach - addressed at Arabic speaking members of civil society we met during a Digital Security Clinic or an event

### Body


عزيزي {{ beneficiary name }}


هذا {{ incident handler name }} من منظمة أكسس ناو. لقد كان من دواعي سروري لقاؤك خلال
{{ event name }}


أود من خلال هذه الرسالة التعبير عن مدى استمتاعي بالتحدث إليك أثناء ذلك اللقاء. فريق السلامة المعلوماتية متجند خلال كامل ساعات اليوم على مدى كامل الأسبوع لتوفير المساعدة التقنية والنظرية في مايخص الحماية الرقمية أو التصدي للهجمات السيبرانية. يمكن لك أن تقرأ أكثر عن خدماتنا من خلال الرابط التالي:

https://www.accessnow.org/help-ar/


كما تجد مفتاح التشفير
PGP
 مرفق لهذه الرسالة.

التشفير يساعد على حماية المحادثات بالكامل وبصفة مستقلة عن مزود الخدمة. سوف نكون سعداء بمساعدتك في تنصيب هذه التقنية.


كما عبرت عن ذلك سلفاً، سوف نكون جاهزين لمساعدتك متى إقتضت الحاجة.


شكراً،  

{{ incident handler name }}


* * *

### Related Articles

- [Article #142: Outreach Template](142-Outreach_Template.html)

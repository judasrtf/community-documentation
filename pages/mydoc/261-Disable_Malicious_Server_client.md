---
title: Disable C&C Server - Email to Client
keywords: C&C server, malware, email templates, malicious website
last_updated: August 12, 2021
tags: [vulnerabilities_malware_templates, templates]
summary: "Email to ask the client or partner for permission to share malware indicators with the digital security community after a C&C server has been disabled"
sidebar: mydoc_sidebar
permalink: 261-Disable_Malicious_Server_client.html
folder: mydoc
conf: Public
ref: Disable_Malicious_Server_client
lang: en
---


# Disable C&C Server - Email to Client
## Template for ask the client for permission to share the malware indicators with the digital security community

### Body

Dear {{ beneficiary name }},

My name is {{ incident handler name }}. I am part of Access Now's Digital Security Helpline team - https://accessnow.org/help. I'm writing to let you know that we are in process of dealing with your request and we need some information that will help resolve the issue.

I am asking for your permission to share indicators about the malware and Command & Control server associated with it within our community. Sharing the malware and indicators will make the malware less effective and protect others who may be targeted.

We will be looking forward to your reply to move forward on this case.

Thank you,

{{ incident handler name }}


* * *


### Related Articles

- [Article #219: Targeted Malware: Disable Malicious C&C Server](219-Targeted_Malware_Disable_Malicious_Server.html)

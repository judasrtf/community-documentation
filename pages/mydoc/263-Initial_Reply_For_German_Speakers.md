---
title: Initial Reply - For German Speakers
keywords: email templates, initial reply, case handling policy
last_updated: August 12, 2021
tags: [helpline_procedures_templates, templates]
summary: "First response, Email to Client for German speakers"
sidebar: mydoc_sidebar
permalink: 263-Initial_Reply_For_German_Speakers.html
folder: mydoc
conf: Public
lang: de
---


# Initial Reply - For German Speakers
## First response, Email to Client for German speakers

### Body


[[ masculine ]] Sehr geehrter {{ beneficiary name }},

[[ feminine or plural ]] Sehr geehrte {{ beneficiary name }},

Danke für Ihre Anfrage an die Digital Security Helpline von Access Now (https://accessnow.org/help). Mein Name ist {{ incident handler name }} und ich bin hier, um Ihnen zu helfen.

Wir haben Ihre Anfrage bekommen und mein Team arbeitet daran.

Bitte nehmen Sie Rücksicht darauf, dass unsere Helpline von einer Gruppe von Sicherheitsexperten, die in unterschiedlichen Zeitzonen arbeiten, betreut wird. Deswegen können Ihre Meldungen von den verschiedenen Mitgliedern unseres Teams beantwortet werden, je nachdem wann wir die Nachricht empfangen. In Kürze melden wir uns bei Ihnen, um Ihre Anfrage genauer zu besprechen.

Bitte lassen Sie “[accessnow #{{ ticket id }}]” im Betreff aller Meldungen bezüglich Ihrer Anfrage. Das ist eine Referenz-ID, die von unserem Ticketingsystem generiert wurde, um alle Aufgaben bezüglich Ihrer Anfrage zu koordinieren.

Mit freundlichen Grüßen,

{{ incident handler name }}


* * *


### Related Articles

- [Article #17: Initial Reply in English](17-Initial_Reply.html)
- [Article #154: FAQ - Initial Reply](154-FAQ-Initial_Reply.html)

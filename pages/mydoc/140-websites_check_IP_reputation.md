---
title: Online Tools to Check a Websites' Reputation
keywords: forensics, IP address, reputation, web search, lookup, domain name, DNS, URL
last_updated: February 19, 2020
tags: [forensics, articles]
summary: "A task requires you to check the reputation of an IP address, domain name and/or URL."
sidebar: mydoc_sidebar
permalink: 140-websites_check_IP_reputation.html
folder: mydoc
conf: Public
lang: en
---


# Online Tools to Check a Websites' Reputation
## A list of tools that can be used to verify the reputation of IP addresses, domain names, and URLs

### Problem

There are several scenarios where you may need to perform a lookup to check the reputation of an IP address or domain name:

- You are investigating a malicious email (e.g. a phishing email). The sender's IP address can be found in the email header or a link is available.

- You want to check the reputation of an IP address for digital forensics (e.g. an IP address found in a malicious script).

- Access to a website from a particular IP is being subjected to CloudFlare's challenge page (CAPTCHA), as captured by CloudFlare's Web Application Firewall.

- You need to check the history/background of an IP address or a domain name  that has been blacklisted due to involvement in malicious activities.

- You need to check the reputation of an IP address and/or domain name as part of a research or project implementation.


* * *


### Solution

#### IMPORTANT NOTES

1. Do not interact with the investigated IP or domain directly, for example by visiting it, pinging it, or scanning it with Nmap. Scanning a domain or IP address even with an online tool such as wpscan.org can have consequences like blacklisting the IP address of our VPN and/or other undesired legal consequences.

2. Most of these tools require login and every search will be logged. So please assess the security of your activities every time you are starting a search to avoid leaking personal identifiable information on our beneficiaries. You may need to use the Tor browser, but this could lead to CAPTCHA verification.   

#### List of tools

The following online tools can be used to check the reputation of an IP address, URL and domain name. It is recommended to check those indicators in more than one of the sites below for comparison purposes, in order to come up with a more reliable check result.

- Check if the domain is flagged on [CiviCERT's MISP instance](https://misp.civicert.org) following the instructions in [Article #354: Search in CiviCERT's MISP Instance](354-MISP_Search.html).

- Submit the link on [CIRCL's URL ABUSE](https://circl.lu/urlabuse/). This service performs multiple tests in order to review the security of a website:
    - Redirect link analysis (checks where the redirections are pointing to and reviews each entry)
    - Checking each link against various security black lists (like VirusTotal and Google SafeBrowsing) and the EU Phishing Initiative
    - Reviewing the current known associated DNS entries using CIRCL Passive DNS
    - Reviewing the current known associated SSL certificate using CIRCL Passive SSL
    - Reviewing the classification of the ISP hosting the links via CIRCL BGP Ranking

- Check [VirusTotal](https://www.virustotal.com/gui/home/search) for any suspicious relations for this indicator (IP or domain), such as:
    -   Passive DNS Replication: domains that have resolved to the investigated IP over time
    -   URLs that have been hosted in the IP or Domain
    -   Downloaded files: file that have been downloaded from the investigated IP or domain
    -   Referring files: files that contained the investigated IP or domain
    -   Communicating Files: files that communicated with the investigated IP or Domain upon execution
    -   Historical Whois Lookups and SSL Certificates: similar to the CIRCL's URL Abuse above
    -   Dig more into relations that have scores with VirusTotal and or have been sandboxed using one of their tools 

- [Urlscan.io](Urlscan.io) is a private company, so it should be used in *private mode* to scan URLs. The scanning process will visit the web page and record all redirections to the effective URL, IPs contacted, scripts and files downloaded. The tool has features to show known malicious files, domains or IPs, but you would need to investigate any indicator separately. For example, if you find that a linked malicious file is located at `example.com/file.exe`, you should double-check the reputation of the effective domain (`example.com`) with other tools listed in this article.

- [Shodan.io](Shodan.io) can be helpful when we investigate an IP address. It is continuously and passively scanning IPs for open ports, services, installed technologies and associated vulnerabilities.

- Malicious domains are usually not included in the 1 million top domains. Check the domain against [Alexa Top Sites](https://www.alexa.com/siteinfo). If it does not exist there, then this is an indicator that the domain has a poor reputation and could be malicious. 

- Check the IP address or the domain with the following tools:
    - [RiskIQ Community Edition](https://community.riskiq.com/) (you will need to sign up to the free edition, which offers up to 25 queries per day)
    - [MX ToolBox](http://mxtoolbox.com/blacklists.aspx)
    - [CISCO Senderbase](http://www.senderbase.org/)
    - [McAfee TrustedSource](https://trustedsource.org/)
    - [Spamhaus](https://www.spamhaus.org/lookup/)
    - [Project Honeypot](https://www.projecthoneypot.org/search_ip.php)
    

* * *


### Comments

There are other trusted websites which can be used to check the reputation of IP addresses. The list above shall be updated as necessary.


* * *

### Related Articles

- [Article #354: Search in CiviCERT's MISP Instance](354-MISP_Search.html)

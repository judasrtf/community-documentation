---
title: Email to platform with no existing escalation process
keywords: email template, Account Recovery, Account security, Data leaks, Harassment
last_updated: July 14, 2022
tags: [account_recovery_templates, templates]
summary: "Template for writing to platforms with no established contact procedure"
sidebar: mydoc_sidebar
permalink: 430-Email_to_platform_with_no_existing_escalation_process.html
folder: mydoc
conf: Public
ref: Email_to_platform_with_no_existing_escalation_process
lang: en
---


# Email to platform with no existing escalation process
## Template for writing to platforms with no established contact procedure

### Body

Dear {{ beneficiary name }},

My name is {{ incident handler name }}. I am part of Access Now's Digital Security Helpline team - https://accessnow.org/help. We are a [computer emergency response team](https://www.first.org/members/teams/access_now_digital_security_helpline) dedicated for civil society that offers real-time, direct technical assistance, and advice to civil society around the world. I'm respectfully contacting you to ask for your assistance with a digital security issue that was recently reported to our Helpline.

[[ State the situation or incident here ]]

[[ Helpful information to include: ]]
    
- [[ The background work of our beneficiary (links to client website or trusted external sites e.g. UN, citation from CiviCERT members website) ]]
- [[ Situation we are dealing with (current events would not need further information, however for incident that needs explanation of context, links would also help) ]]
- [[ Request that needs to be processed (e.g. removal of content) along side with support ticket from standard reporting mechanism of platform ]]

Please, don't hesitate to get back to us if you have further questions or need additional information.

We would appreciate it if you could confirm the receipt of this report.

With best regards,
{{ incident handler name }}

* * *


### Related Articles

- [Article #23: FAQ - Account Recovery and Deactivation](23-FAQ-Account_Recovery_and_Deactivation.html)
- [Article #428: Escalations To Platforms With No Escalation Channel](428-Escalations_To_Platforms_With_No_Escalation_Channel.html)


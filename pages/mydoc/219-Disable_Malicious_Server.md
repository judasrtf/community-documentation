---
title: Report and Disable Malicious C&amp;C Server
keywords: targeted attacks, C&amp;C server, malware, phishing
last_updated: August 8, 2019
tags: [vulnerabilities_malware, articles]
summary: "A C&amp;C server is identified by malware researchers, clients, or partners to be linked to a phishing campaign or to malware targeting civil society groups; victims may be receiving malicious files through social media and/or chat tools; control servers may also hop from DNS domain to DNS domain"
sidebar: mydoc_sidebar
permalink: 219-Disable_Malicious_Server.html
folder: mydoc
conf: Public
ref: Disable_Malicious_Server
lang: en
---


# Report and Disable Malicious C&amp;C Server
## What to do when a C&amp;C server is used to spread malware or for a phishing campaign

### Problem

- When a website is being used for a phishing campaign or for spreading malware, it is important to take it down to stop it from harming more people.
- If the C&amp;C server is not disabled, the malware will continue to infect more devices and makes more victims from the targeted civil society community in danger.
- If partner organizations have covert access to the C&amp;C server, they may be able to find out who gets compromised in the community and notify them.


* * *


### Solution

1. Make sure you have the minimal information necessary for this case, at minimum one of the following:
    - IP of C&amp;C server (ideally with the port that is used by the C&amp;C server)
    - Sample of the malware
    - URL pointing to the C&amp;C server

    This information can come from the partner or beneficiary sharing information about the C&amp;C server or malicious website.
    
2. Paste the URL in [this page](https://www.circl.lu/urlabuse/) to receive all the contact points from the whois entries.

3. Send to *all contacts* listed in point 2 an email notifying them of the malicious
content found on one of their domains. An example of what this email could look like may be found in [Article #209: Template - Phishing Link](209-Phishing_Link.html).

4. Ask the client/partner for permission to share the indicators of compromise with the digital security community.

    You may use the template included in [Article 261: Disable C&amp;C server - email to client](261-Disable_Malicious_Server_client.html).

5. When permission from client to share the indicators has been obtained, add them to MISP following the instructions in [Article #355: How to Add an Event to CiviCERT's MISP Instance](355-MISP_Add_Event.html).
7. To disable the domain, contact immediately:
    - The registrar of the malicious domain to disable it
    - The hosting provider to disable the IP and malicious content

    You may use the following templates when contacting them: 
    
    - **Template to contact the registrar: [Article 259: Disable C&amp;C server - email to registrar of malicious domain](259-Disable_Malicious_Server_registrar.html)**
    - **Template to contact the hosting provider: [Article 260: Disable C&amp;C server - email to hosting provider](260-Disable_Malicious_Server_hosting_provider.html)**


* * *


### Comments

Also see [Article #258: Advanced Threats Triage Workflow](258-Advanced_Threats_Triage_Workflow.html).


* * *

### Related Articles

- [Article #133: How to clean a malware-infected Windows machine](133-Clean_Malware_Windows.html)
- [Article #209: Template - Phishing Link](209-Phishing_Link.html)
- [Article #258: Advanced Threats Triage Workflow](258-Advanced_Threats_Triage_Workflow.html)
- [Article #259: Disable C&amp;C server - email to registrar of malicious domain](259-Disable_Malicious_Server_registrar.html)
- [Article #260: Disable C&amp;C server - email to hosting provider](260-Disable_Malicious_Server_hosting_provider.html)
- [Article #261: Disable C&amp;C server - email to client](261-Disable_Malicious_Server_client.html)
- [Article #355: How to Add an Event to CiviCERT's MISP Instance](355-MISP_Add_Event.html)

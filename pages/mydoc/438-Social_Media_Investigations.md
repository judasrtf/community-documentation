---
title: Resources for conducting investigations on Social Media
keywords: OSINT, social media, bots, coordinated malicious activity, inauthentic behavior
last_updated: November 2, 2022
tags: [account_security, articles]
summary: "Resources for conducting open source investigations of malicious behavior or accounts on social media."
sidebar: mydoc_sidebar
permalink: 438-Social_Media_Investigations.html
folder: mydoc
conf: Public
ref: Social_Media_Investigations
lang: en
---


# Resources for conducting investigations on Social Media
## Resources for conducting open source investigations of malicious behavior on social media

### Problem

Social media platforms are sometimes used by adversaries to target Helpline beneficiaries. Examples of attacks include harassment campaigns, disinformation campaigns, and mass bot following. In most cases, platforms will be in the best position to investigate and take action to contain any attacks. However, in some instances, including when platforms lack capacity and interest or when they are not being effective, we can take advantage of existing tools to conduct research by our own means through Open Source Intelligence (OSINT) or Social Media Intelligence (SOCMINT).

Please be careful when utilizing the tools below, or when searching for new tools on the Internet. There are hundreds of malicious applications that promise to provide private information about profiles and users, but which are actually designed to steal sensitive information from those who use them.

Moreover, it is important to be mindful of the ethics behind conducting investigations on social media. Further considerations on this are detailed below.

* * *

### Solution

#### Ethics of OSINT and SOCMINT

Investigations should only be conducted using publicly available data on social networking sites, that is, information publicly available to anyone when they are not logged in. Investigations, even if centered only on publicly available information, can be an intrusion into people’s privacy and therefore must comply with the international principles of legality, necessity, and proportionality.

Some social media companies have included provisions in their Terms of Service to ban some of the techniques used to collect data, such as “scraping”. Therefore, an analysis of the investigation approach should be conducted before engaging in research.

Before conducting an investigation please consult with the Access Now Legal team (legal@accessnow.org) for guidance and practical considerations.

#### Tools

This article provides a list of tools that can be used to gather openly available information to conduct investigations. Please note that the Helpline has not done first-hand testing of these resources (unless specified), and this is also not a comprehensive list of every tool or method that exists. When doing research, it is suggested that you work together with a member of the Helpline analysis team.

#### Twitter

- [accountanalysis.app](https://accountanalysis.app/about) - analyze a twitter account, interactions with accounts, and stats
- [tw1tter0s1nt](https://github.com/falkensmz/tw1tter0s1nt) - python script for analysis, has a few modules to research followers, links, keywords
- [Foller.me](https://foller.me/) - Account analysis tool, no sign-in required. Example: https://foller.me/beehaber
- [TwitWork](https://github.com/atmoner/TwitWork#what-is-twitwork) - creates graphs and maps of accounts and hashtags for real-time analysis
- [twitter-followers](https://github.com/ConradIrwin/twitter-followers) - download twitter followers
- [Bot Sentinel](https://botsentinel.com/info/about) - Bot Sentinel is a platform designed to help fight disinformation and targeted harassment. The platform uses machine learning and artificial intelligence to classify Twitter accounts and add the accounts to a publicly available database that anyone can browse.

#### Instagram

- [Instaloader (by Alexander Graf)](https://instaloader.github.io/) -  Python library tool to download pictures (or videos) along with their captions and other metadata from Instagram. Using Instaloader it is possible to download public and private profiles, hashtags, user stories, feeds and saved media. It can also download comments, geotags and captions of each post. The Helpline doesn't have experience with the use of this tool, but Qurium referred to it in [this report](https://www.qurium.org/alerts/iran/weaponizing-instagram-against-the-iranian-metoo/).


#### Facebook

- [Who Posted What?](https://whopostedwhat.com/) - Facebook tools to identify profile IDs. It allows you to search keywords on specific dates.

#### TikTok

- [This guide](https://www.skopenow.com/resource-center/tiktok-investigations-and-osint-tips) by Jake Creps explains how to discover profiles on TikTok, extract content from profiles, apply analysis to the information found, and expand an investigation from TikTok and beyond, all within the constraints of a web browser. No mobile phones or emulators are required.

- https://ciberpatrulla.com/osint-tiktok/ (In Spanish) - Guide by Julián Gutiérrez with information on how to do a detailed analysis of a TikTok profile, how to download profile pictures and videos and tips to uncover connections between profiles.

#### General Resources

- [MALTEGO - Handbook for Social Media Investigations](https://static.maltego.com/cdn/Handbooks/Maltego-Handbook-for-Social-Media-Investigations-Short.pdf): Standard Investigative Workflows Departing from Names, Aliases, Email Addresses, Images, and more.

* * *


### Comments




* * *

### Related Articles
